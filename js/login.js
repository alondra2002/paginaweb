// configuracion firebase 

//Import the functions you nedd from the SDKs you need 
import { initializeApp}
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import{getDatabase,onValue,ref,set,child,get,update,remove}
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import{getStorage, ref as refS, uploadBytes, getDownloadURL} 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDTcUqwUS0P7B16q2xcflUWGvSxmSsULyA",
    authDomain: "shopsotoos.firebaseapp.com",
    projectId: "shopsotoos",
    databaseURL: "https://shopsotoos-default-rtdb.firebaseio.com/",
    storageBucket: "shopsotoos.appspot.com",
    messagingSenderId: "748874462017",
    appId: "1:748874462017:web:a9b1ceab8ac7366ee1e7c0"
  };
  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase(); 

  var btnLogin= document.getElementById('btnLogin');
  var btnLimpiar= document.getElementById('btnLimpiar');

  function login(){
    let email = document.getElementById('Usuario').value;
    let contra= document.getElementById('contra').value;

    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, contra)
    .then((userCredential) => {
    // Signed in 
    const user = userCredential.user;
    location.href="/FINAL/html/admin.html"
    // ...
     })
    .catch((error) => {
    location.href="/FINAL/html/error.html";
    });
  }
    function limpiar(){
        document.getElementById('Usuario').value="";
        document.getElementById('contra').value="";
    }
  btnLogin.addEventListener('click', login);
  btnLimpiar.addEventListener('click', limpiar);