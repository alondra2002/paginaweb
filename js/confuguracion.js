// configuracion firebase 

//Import the functions you nedd from the SDKs you need 
import { initializeApp}
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import{getDatabase,onValue,ref,set,child,get,update,remove}
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import{getStorage, ref as refS, uploadBytes, getDownloadURL} 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDTcUqwUS0P7B16q2xcflUWGvSxmSsULyA",
    authDomain: "shopsotoos.firebaseapp.com",
    projectId: "shopsotoos",
    databaseURL: "https://shopsotoos-default-rtdb.firebaseio.com/",
    storageBucket: "shopsotoos.appspot.com",
    messagingSenderId: "748874462017",
    appId: "1:748874462017:web:a9b1ceab8ac7366ee1e7c0"
  };
  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase(); 

  //declaracion de objetos
  var btnInsertar = document.getElementById('btnInsertar');
  var btnBuscar = document.getElementById('btnBuscar');
  var btnActualizar = document.getElementById('btnActualizar');
  var btnBorrar = document.getElementById('btnBorrar');
  var btnLimpiar = document.getElementById('btnLimpiar');
  var lista = document.getElementById('lista');
  var btnTodos = document.getElementById('btnTodos');
  var archivo = document.getElementById('archivo');
  var btnMostrarImagen = document.getElementById('verImagen'); 


  //insertar
  var matricula = "";
  var nombre = "";
  var carrera = "";
  var genero = "";
  
function leerInputs(){

    matricula = document.getElementById('matricula').value; 
    nombre = document.getElementById('nombre').value;
    carrera = document.getElementById('carrera').value; 
    genero = document.getElementById('genero').value;

    //alert (" matricula " + matricula + " nombre " + nombre + " carrera " + carrera + " genero " + genero)
}

function InsertarDatos(){
    leerInputs(); 

    set(ref(db, 'alumnos/' + matricula), {
        nombre: nombre, 
        carrera: carrera, 
        genero: genero})
        
        .then((docRef)=>{
        alert("se agrego el registro con exito"); 
        mostrarAlumnos();
        console.log("datos" + matricula + nombre + carrera+ genero)

    })
    .catch((error)=>{
        alert("surgio un error " + error)
    });
    alert("se agrego con exito");
};

function mostrarDatos(){
    leerInputs();
    const dbref = ref(db);

    get(child(dbref, 'alumnos/'+matricula)).then((snapshot)=>{

        if(snapshot.existes()){
            nombre = snapshot.val().nombre;
            carrera = snapshot.val().carrera;
            genero = snapshot.val().genero;
            escribirInput();
        }

        else{
            alert("No exisite la matricula");
        }
    }).catch((error)=>{

        alert("Ocurrió un error" + error);
    })
 }

 function escribirInput(){
    document.getElementById('matricula').value;
    document.getElementById('nombre').value;
    document.getElementById('carrera').value;
    document.getElementById('genero').value;

 }
 function limpiar(){
    lista.innerHTML="";
    matricula="";
    nombre="";
    carrera="";
    genero=1;
    escribirInput();
   }

        
//mostrar alumnos 
function mostrarAlumnos(){
    const db = getDatabase();
    const dbRef = ref(db, 'alumnos'); 

    onValue(dbRef,(snapshot) => {
        lista.innerHTML="";
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();

            lista.innerHTML= "<div class= 'campo'> " 
            + lista.innerHTML + childKey + " " + childData.nombre
             + " " +childData.carrera + " " + childData.genero + "<br> </div>";
        });
        {
            onlyOnce: true
        }
    });
  
}
//Actualizar
function actualizar(){
    leerInputs();

    update(ref(db, 'alumnos/' + matricula),{
        nombre:nombre,
        carrera:carrera,
        genero:genero
    }).then(()=>{
        alert("Se realizó actualizacion");
        mostrarAlumnos();
    }).catch(() => {
        alert("Surgio un error" + error);
    });
  }
  //borrar
  function borrar(){
    leerInputs();

    remove(ref(db, 'alumnos/' + matricula)).then(() =>{
        alert("se borro");
        mostrarAlumnos();
    }).catch(() => {
        alert("surgio un error" + error);
    })
  }

function cargarImagen(){
    //archivo seleccionado 
    const file = event.target.file[0];
    const name = event.target.file[0]; 

const storage = getStorage();
const storageRef = refS(storage, 'imagen/' + name);

// 'file' comes from the Blob or File API
uploadBytes(storageRef, file).then((snapshot) => {
    document.getElementById('imgNombre').value = name;
  alert('Se cargo el archivo');
});
}

function descargarImagen(){
    archivo = document.getElementById('imgNombre').value
  const storage = getStorage();
  const storageRef = refS(storage, 'imagenes/' + archivo);

  // Get the download URL
  getDownloadURL(storageRef)
    .then((url) => {
      document.getElementById('url').value = url;
      document.getElementById('imagen').src = url
    })
    .catch((error) => {
      switch (error.code) {
        case 'storage/object-not-found':
          console.log('No existe el archivo')
          break;
        case 'storage/unauthorized':
          console.log('No tiene permisos')
          break;
        case 'storage/canceled':
          console.log('Se cancelo o no tiene internet')
          break;
        case 'storage/unknown':
          console.log('Surgio al inesperado')
          break;
      }
    });
} 
//evento click 
btnInsertar.addEventListener('click', InsertarDatos)
btnBuscar.addEventListener('click', mostrarDatos);
btnTodos.addEventListener('click', mostrarAlumnos);
btnLimpiar.addEventListener('click',limpiar);
btnActualizar.addEventListener('click', actualizar);
btnBorrar.addEventListener('click',borrar); 
 archivo.addEventListener('change', cargarImagen);
 btnMostrarImagen.addEventListener('click', descargarImagen); 